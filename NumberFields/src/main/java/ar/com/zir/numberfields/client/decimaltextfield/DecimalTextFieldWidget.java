package ar.com.zir.numberfields.client.decimaltextfield;

import com.google.gwt.dom.client.Element;
import com.vaadin.client.ui.VTextField;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
public class DecimalTextFieldWidget extends VTextField {

    public DecimalTextFieldWidget() {
        super();
        // CSS class-name should not be v- prefixed
        addStyleName("numberfield");

        // State is set to widget in DecimalTextFieldConnector		
    }

    public DecimalTextFieldWidget(Element node) {
        super(node);
        addStyleName("numberfield");
    }

}