package ar.com.zir.numberfields.client.decimaltextfield;

import ar.com.zir.numberfields.shared.DecimalFormat;
import com.vaadin.shared.ui.textfield.AbstractTextFieldState;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
public class DecimalTextFieldState extends AbstractTextFieldState {

    public DecimalFormat format;

    private static final long serialVersionUID = -3770632874605735616L;
	
}