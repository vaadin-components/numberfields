/*
 * Copyright 2015 Juan Martin Runge <jmrunge@gmail.com>.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.zir.numberfields.shared;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
public class DecimalFormatException extends IllegalArgumentException {

    public DecimalFormatException(String message) {
        super(message);
    }

    public DecimalFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public DecimalFormatException(Throwable cause) {
        super(cause);
    }

}
