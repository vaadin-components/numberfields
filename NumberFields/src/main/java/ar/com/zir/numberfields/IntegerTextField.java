/*
 * Copyright 2015 Juan Martin Runge {@literal <jmrunge@gmail.com>}.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.zir.numberfields;

import ar.com.zir.numberfields.client.integertextfield.IntegerTextFieldState;
import com.vaadin.ui.TextField;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 * This is the server-side UI component that provides public API 
 * for IntegerTextField
 * 
 */
public class IntegerTextField extends TextField {
    
    private void log(String message) {
        Logger.getLogger(this.getClass().getName()).log(Level.FINE, message);
    }

    public IntegerTextField() {
        super();
    }
    
    public IntegerTextField(String caption) {
        super(caption);
    }
    
    // We must override getState() to cast the state to DecimalTextFieldState
    @Override
    public IntegerTextFieldState getState() {
        return (IntegerTextFieldState) super.getState();
    }
        
//    @Override
//    public void setPropertyDataSource(Property newDataSource) {
//        PropertyFormatter pf = new PropertyFormatter(newDataSource) {
//
//            @Override
//            public String format(Object value) {
//                return getDecimalFormat().format((BigDecimal)value);
//            }
//
//            @Override
//            public Object parse(String formattedValue) throws Exception {
//                return getDecimalFormat().parse(formattedValue);
//            }
//        };
//        super.setPropertyDataSource(pf);
//    }
    
    @Override
    public void setValue(String value) {
        log("Value received by setValue: " + value);
        if (value != null && value.length() > 0) 
            value = getValueFromString(value);
        super.setValue(value);
    }

    public void setIntegerValue(Integer value) {
        log("Value received by setIntegerValue: " + value);
        String val = null;
        if (value != null) 
            val = getValueFromInteger(value);
        super.setValue(val);
    }

    @Override
    public void setValue(String value, boolean repaintIsNotNeeded) {
        log("Value received by 'internal' setValue: " + value);
        if (value != null && value.length() > 0) 
            value = getValueFromString(value);
        super.setValue(value, repaintIsNotNeeded);
    }

    private String getValueFromInteger(Integer value) {
        String formated = value.toString();
        if (formated.trim().length() > getState().maxIntegers)
            formated = formated.substring(formated.trim().length() - getState().maxIntegers);
        log("Value to return by getValueFromBigDecimal: " + formated);
        return formated;
    }
    
    private String getValueFromString(String value) {
        if (value.trim().length() > getState().maxIntegers)
            value = value.substring(value.trim().length() - getState().maxIntegers);
        Integer parsed = new Integer(value);
        log("Value parsed in getValueFromString: " + parsed);
        return parsed.toString();
    }
    
    public Integer getIntegerValue() {
        String value = getValue();
        if (value.trim().length() > getState().maxIntegers)
            value = value.substring(value.trim().length() - getState().maxIntegers);
        Integer parsed = new Integer(value);
        log("Value to return by getBigDecimalValue: " + parsed);
        return parsed;
    }
    
    public void setMaxIntegers(int maxIntegers) {
        if (maxIntegers < 1)
            throw new NumberFormatException("Max Integers must be greater than 0");
        getState().maxIntegers = maxIntegers;
    }

    public int getMaxIntegers() {
        return getState().maxIntegers;
    }

}