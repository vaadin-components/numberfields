/*
 * Copyright 2015 Juan Martin Runge {@literal <jmrunge@gmail.com>}.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.zir.numberfields.validator;

import java.math.BigDecimal;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
public class DecimalValidator implements ConstraintValidator<Decimal, Object> {
    private int fraction;
    private int integer;
    private String decimalSeparator;
    
    @Override
    public void initialize(Decimal decimal) {
        this.fraction = decimal.fraction();
        this.integer = decimal.integer();
        this.decimalSeparator = decimal.decimalSeparator();
    }
    
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext constraintContext) {
        if (obj == null) return true;
        String object = obj.toString();
        if (obj instanceof BigDecimal)
            object = ((BigDecimal)obj).toPlainString();
        String integers = "";
        String decimals = "";
        boolean isInt = true;
        for (int i = 0; i < object.length(); i++) {
            if (Character.isDigit(object.charAt(i))) {
                if (isInt)
                    integers += object.charAt(i);
                else
                    decimals += object.charAt(i);
            }
            else if (object.charAt(i) == decimalSeparator.charAt(0))
                isInt = false;
        }
        if (integers.length() > integer)
            return false;
        else return decimals.length() <= fraction;
    }
}
