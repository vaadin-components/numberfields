    package ar.com.zir.numberfields.client.decimaltextfield;

import ar.com.zir.numberfields.shared.DecimalFormat;
import ar.com.zir.numberfields.DecimalTextField;

import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.textfield.TextFieldConnector;
import com.vaadin.shared.ui.Connect;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 * 
 * Connector binds client-side widget class to server-side component class
 * Connector lives in the client and the @Connect annotation specifies the
 * corresponding server-side component
 */

@Connect(DecimalTextField.class)
public class DecimalTextFieldConnector extends TextFieldConnector {
    private String oldVal;
    private char lastChar;
    private DecimalFormat decimalFormat;

    private void log(String message) {
        Logger.getLogger(this.getClass().getName()).log(Level.FINE, message);
    }

    public DecimalTextFieldConnector() {
        getWidget().addKeyPressHandler(new KeyPressHandler() {            
            @Override
            public void onKeyPress(KeyPressEvent event) {
                log("Tecla: " + event.toDebugString());
                log("Text: " + getState().text);
                log("Value: " + getWidget().getValue());
                log("WidgetText: " + getWidget().getText());

                if (!Character.isDigit(event.getCharCode()))
                    getWidget().cancelKey();
                lastChar = event.getCharCode();
            }
        });
        getWidget().addKeyUpHandler(new KeyUpHandler() {
            @Override
            public void onKeyUp(KeyUpEvent event) {
                log("Valor de KeyUp: " + getWidget().getValue());
                String text = getWidget().getValue();
                if (text != null && text.trim().length() > 0) {
                    if ((lastChar == decimalFormat.getGroupingSeparator().charAt(0)) || lastChar == decimalFormat.getDecimalSeparator().charAt(0)) {
                        int pos = getWidget().getCursorPos();
                        if (text.substring(pos, pos + 1).equals(decimalFormat.getGroupingSeparator()) && (lastChar == decimalFormat.getGroupingSeparator().charAt(0))) {
                            pos++;
                            getWidget().setCursorPos(pos);
                        } else if (text.substring(pos, pos + 1).equals(decimalFormat.getDecimalSeparator()) && (lastChar == decimalFormat.getDecimalSeparator().charAt(0))) {
                            pos++;
                            getWidget().setCursorPos(pos);
                        }
                    }
                    if (oldVal == null || (oldVal != null && !oldVal.equals(text))) {
                        int pos = getWidget().getCursorPos();
                        if (text.contains(decimalFormat.getDecimalSeparator()) && text.substring(text.indexOf(decimalFormat.getDecimalSeparator()) + 1).length() > decimalFormat.getDecimals()) {
                            log("Elimino decimales excedentes");
                            pos = pos - (text.length() - oldVal.length());
                            text = oldVal;
                        }
                        String intValue = text;
                        if (text.contains(decimalFormat.getDecimalSeparator()))
                            intValue = intValue.substring(0, text.indexOf(decimalFormat.getDecimalSeparator()));
                        log("Parte entera: " + intValue);
                        if (intValue.replace(decimalFormat.getGroupingSeparator(), "").length() > decimalFormat.getMaxIntegers()) {
                            log("Elimino enteros excedentes");
                            pos = pos - (text.length() - oldVal.length());
                            text = oldVal;
                        }
                        String oldText = text.substring(0, pos);
                        int oldLength = text.length();
                        log("Texto anterior hasta cursor pos: " + oldText);
                        BigDecimal val = decimalFormat.parse(text);
                        text = decimalFormat.format(val);
                        String newText = text.substring(0, pos);
                        int newLength = text.length();
                        log("Texto posterior hasta cursor pos: " + newText);
                        if (!oldText.equals(newText) && (oldLength != newLength))
                            pos++;
                        log("Valor a setear en widget: " + text);
                        getWidget().setValue(text);
                        log("Cursor pos a setear en widget: " + pos);
                        getWidget().setCursorPos(pos);
                    }
                }
                oldVal = text;
                lastChar = "z".charAt(0);
            }
        });
    }
        
    // We must implement createWidget() to create correct type of widget
    @Override
    protected Widget createWidget() {
        return GWT.create(DecimalTextFieldWidget.class);
    }

	
    // We must implement getWidget() to cast to correct type
    @Override
    public DecimalTextFieldWidget getWidget() {
        return (DecimalTextFieldWidget) super.getWidget();
    }

    // We must implement getState() to cast to correct type
    @Override
    public DecimalTextFieldState getState() {
        return (DecimalTextFieldState) super.getState();
    }

    // Whenever the state changes in the server-side, this method is called
    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);

        // State is directly readable in the client after it is set in server
        boolean changeValue = false;
        String text = getWidget().getValue();
        if (stateChangeEvent.hasPropertyChanged("format")) {
            decimalFormat = getState().format;
            changeValue = true;
        }
        if (stateChangeEvent.hasPropertyChanged("text")) {
            text = getState().text;
            changeValue = true;
        }
        if (changeValue) {
            text = decimalFormat.format(decimalFormat.parse(text));
            getWidget().setValue(text);
            oldVal = text;
        }
    }

}
