/*
 * Copyright 2015 Juan Martin Runge {@literal <jmrunge@gmail.com>}.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.zir.numberfields.client.integertextfield;

import ar.com.zir.numberfields.IntegerTextField;
import com.google.gwt.core.client.GWT;
import com.google.gwt.event.dom.client.KeyPressEvent;
import com.google.gwt.event.dom.client.KeyPressHandler;
import com.google.gwt.user.client.ui.Widget;
import com.vaadin.client.communication.StateChangeEvent;
import com.vaadin.client.ui.textfield.TextFieldConnector;
import com.vaadin.shared.ui.Connect;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 *
 * Connector binds client-side widget class to server-side component class
 * Connector lives in the client and the @Connect annotation specifies the
 * corresponding server-side component
*/

@Connect(IntegerTextField.class)
public class IntegerTextFieldConnector extends TextFieldConnector {

    private void log(String message) {
        Logger.getLogger(this.getClass().getName()).log(Level.FINE, message);
    }

    public IntegerTextFieldConnector() {
        getWidget().addKeyPressHandler(new KeyPressHandler() {            
            @Override
            public void onKeyPress(KeyPressEvent event) {
                log("Tecla: " + event.toDebugString());
                log("Text: " + getState().text);
                log("Value: " + getWidget().getValue());
                log("WidgetText: " + getWidget().getText());

                if (!Character.isDigit(event.getCharCode()) || getWidget().getText().length() == getState().maxIntegers)
                    getWidget().cancelKey();
            }
        });
    }
        
    // We must implement createWidget() to create correct type of widget
    @Override
    protected Widget createWidget() {
        return GWT.create(IntegerTextFieldWidget.class);
    }

	
    // We must implement getWidget() to cast to correct type
    @Override
    public IntegerTextFieldWidget getWidget() {
        return (IntegerTextFieldWidget) super.getWidget();
    }

    // We must implement getState() to cast to correct type
    @Override
    public IntegerTextFieldState getState() {
        return (IntegerTextFieldState) super.getState();
    }

    // Whenever the state changes in the server-side, this method is called
    @Override
    public void onStateChanged(StateChangeEvent stateChangeEvent) {
        super.onStateChanged(stateChangeEvent);

        // State is directly readable in the client after it is set in server
    }

}
