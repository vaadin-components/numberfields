/*
 * Copyright 2015 Juan Martin Runge {@literal <jmrunge@gmail.com>}.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package ar.com.zir.numberfields.shared;

import ar.com.zir.numberfields.shared.DecimalFormatException;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 */
public class DecimalFormat implements Serializable {
    private int groupingSize;
    private String groupingSeparator;
    private String decimalSeparator;
    private int maxIntegers;
    private int decimals;

    private void log(String message) {
        Logger.getLogger(this.getClass().getName()).log(Level.FINE, message);
    }

    public DecimalFormat() {
    }

    public DecimalFormat(int groupingSize, String groupingSeparator, String decimalSeparator, int maxIntegers, int decimals) {
        this.groupingSize = groupingSize;
        this.groupingSeparator = groupingSeparator;
        this.decimalSeparator = decimalSeparator;
        this.maxIntegers = maxIntegers;
        this.decimals = decimals;
    }

    public int getGroupingSize() {
        return groupingSize;
    }

    public void setGroupingSize(int groupingSize) {
        if (groupingSize < 0)
            throw new DecimalFormatException("Grouping Size must be 0 or a possitive value");
        this.groupingSize = groupingSize;
    }

    public String getGroupingSeparator() {
        return groupingSeparator;
    }

    public void setGroupingSeparator(String groupingSeparator) {
        if (groupingSeparator == null)
            groupingSeparator = "";
        if (groupingSeparator.length() > 1)
            throw new DecimalFormatException("Grouping Separator must be a single character or none");
        this.groupingSeparator = groupingSeparator;
    }

    public String getDecimalSeparator() {
        return decimalSeparator;
    }

    public void setDecimalSeparator(String decimalSeparator) {
        if (decimalSeparator == null)
            throw new DecimalFormatException("Decimal Separator must be non null");
        else if (decimalSeparator.length() != 1)
            throw new DecimalFormatException("Decimal Separator must be a single character");
        this.decimalSeparator = decimalSeparator;
    }

    public int getMaxIntegers() {
        return maxIntegers;
    }

    public void setMaxIntegers(int maxIntegers) {
        if (maxIntegers < 1)
            throw new DecimalFormatException("Max Integers must be more than 0");
        this.maxIntegers = maxIntegers;
    }

    public int getDecimals() {
        return decimals;
    }

    public void setDecimals(int decimals) {
        if (decimals < 1)
            throw new DecimalFormatException("Decimals must be more than 0");
        this.decimals = decimals;
    }
    
    public BigDecimal parse(String value) {
        validateFormat();
        String val = value;
        if (val == null || !val.contains(decimalSeparator))
            throw new DecimalFormatException("Invalid value " + val);
        if (groupingSeparator != null)
            val = val.replace(groupingSeparator, "");
        val = val.replace(decimalSeparator, ".");
        return new BigDecimal(val);
    }
    
    private void validateFormat() {
        if (groupingSize > 0 && (groupingSeparator == null || groupingSeparator.trim().length() == 0))
            throw new DecimalFormatException("Grouping Separator must be specified when grouping size is > 0");
        else if (groupingSize > 0 && groupingSeparator.length() > 1)
            throw new DecimalFormatException("Grouping Separator must be a single character");
        else if (decimalSeparator == null || decimalSeparator.trim().length() == 0)
            throw new DecimalFormatException("Decimal Separator must be specified");
        else if (decimalSeparator.length() != 1)
            throw new DecimalFormatException("Decimal Separator must be a single character");
        else if (decimals < 1)
            throw new DecimalFormatException("Decimals must be > 0");
        else if (groupingSize > 0 && groupingSeparator.equals(decimalSeparator))
            throw new DecimalFormatException("Grouping Separator and Decimal Separator must be different characters");
    }
    
    public String format(BigDecimal value) {
        log("Valor a formatear: " + value);
        validateFormat();
        if (value == null)
            throw new DecimalFormatException("Invalid value " + value);
        String text = value.toPlainString();
        String intPart = text;
        String decPart = "0";
        if (text.contains(".")) {
            String[] parts = text.split("\\.");
            intPart = parts[0];
            decPart = parts[1];
        }
        int length = intPart.length();
        if (length > maxIntegers) 
            length = maxIntegers;
        log("Parte entera: " + intPart);
        log("Parte decimal " + decPart);
        String formated = "";
        int group = 0;
        for (int i = length - 1; i >= 0; i--) {
            formated = intPart.charAt(i) + formated;
            log("Formateando entero: " + formated);
            group++;
            if (group == groupingSize) {
                formated = groupingSeparator + formated;
                log("Agregando separador grupos: " + formated);
                group = 0;
            }
        }
        if (formated.startsWith(groupingSeparator))
            formated = formated.substring(1);
        log("Nuevo valor entero: " + formated);
        if (decPart.length() < decimals) {
            for (int i = decPart.length(); i < decimals; i++) {
                decPart += "0";
            }
        } else if (decPart.length() > decimals) {
            decPart = decPart.substring(0, decimals);
        }
        formated = formated + decimalSeparator + decPart;
        log("Nuevo valor final: " + formated);
        return formated;
    }
    
}
