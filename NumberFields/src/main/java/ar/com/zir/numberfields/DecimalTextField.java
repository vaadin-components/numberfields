package ar.com.zir.numberfields;

import ar.com.zir.numberfields.shared.DecimalFormatException;
import ar.com.zir.numberfields.client.decimaltextfield.DecimalTextFieldState;
import ar.com.zir.numberfields.shared.DecimalFormat;
import com.vaadin.data.Property;
import com.vaadin.data.util.PropertyFormatter;

import com.vaadin.ui.TextField;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Juan Martin Runge {@literal <jmrunge@gmail.com>}
 *
 * This is the server-side UI component that provides public API 
 * for DecimalTextField
 * 
 */
public class DecimalTextField extends TextField {
    
    private void log(String message) {
        Logger.getLogger(this.getClass().getName()).log(Level.FINE, message);
    }

    public DecimalTextField() {
        super();
        init();
    }
    
    public DecimalTextField(String caption) {
        super(caption);
        init();
    }
    
    public DecimalTextField(DecimalFormat df) {
        super();
        init(df);
    }
    
    public DecimalTextField(String caption, DecimalFormat df) {
        super(caption);
        init(df);
    }
    
    private void init() {
        init(null);
    }
    
    private void init(DecimalFormat df) {
        if (df == null)
            setDecimalFormat(new DecimalFormat(0, ",", ".", 20, 10));
        else
            setDecimalFormat(df);
    }

    // We must override getState() to cast the state to DecimalTextFieldState
    @Override
    public DecimalTextFieldState getState() {
        return (DecimalTextFieldState) super.getState();
    }
        
    @Override
    public void setPropertyDataSource(Property newDataSource) {
        PropertyFormatter pf = new PropertyFormatter(newDataSource) {

            @Override
            public String format(Object value) {
                return getDecimalFormat().format((BigDecimal)value);
            }

            @Override
            public Object parse(String formattedValue) throws Exception {
                return getDecimalFormat().parse(formattedValue);
            }
        };
        super.setPropertyDataSource(pf);
    }
    
    @Override
    public void setValue(String value) {
        log("Value received by setValue: " + value);
        if (value != null && value.length() > 0) 
            value = getValueFromString(value);
        super.setValue(value);
    }

    public void setBigDecimalValue(BigDecimal value) {
        log("Value received by setBigDecimalValue: " + value);
        String val = null;
        if (value != null) 
            val = getValueFromBigDecimal(value);
        super.setValue(val);
    }

    @Override
    public void setValue(String value, boolean repaintIsNotNeeded) {
        log("Value received by 'internal' setValue: " + value);
        if (value != null && value.length() > 0) 
            value = getValueFromString(value);
        super.setValue(value, repaintIsNotNeeded);
    }

    private String getValueFromBigDecimal(BigDecimal value) {
        String formated = getDecimalFormat().format(value);
        log("Value to return by getValueFromBigDecimal: " + formated);
        return formated;
    }
    
    private String getValueFromString(String value) {
        BigDecimal parsed = getDecimalFormat().parse(value);
        log("Value parsed in getValueFromString: " + parsed);
        String formated = getDecimalFormat().format(parsed);
        log("Value to return by getValueFromString: " + formated);
        return formated;
    }
    
    public BigDecimal getBigDecimalValue() {
        BigDecimal parsed = getDecimalFormat().parse(getValue());
        log("Value to return by getBigDecimalValue: " + parsed);
        return parsed;
    }
    
    public void setMaxIntegers(int maxIntegers) throws DecimalFormatException {
        updateFormat("maxIntegers", Integer.TYPE, maxIntegers);
    }

    public int getMaxIntegers() {
        return getDecimalFormat().getMaxIntegers();
    }

    public void setGroupingSize(int groupingSize) throws DecimalFormatException {
        updateFormat("groupingSize", Integer.TYPE, groupingSize);
    }

    public int getGroupingSize() {
        return getDecimalFormat().getGroupingSize();
    }

    public void setGroupingSeparator(String groupingSeparator) throws DecimalFormatException {
        updateFormat("groupingSeparator", String.class, groupingSeparator);
    }

    public String getGroupingSeparator() {
        return getDecimalFormat().getGroupingSeparator();
    }

    public void setDecimalSeparator(String decimalSeparator) throws DecimalFormatException {
        updateFormat("decimalSeparator", String.class, decimalSeparator);
    }

    public String getDecimalSeparator() {
        return getDecimalFormat().getDecimalSeparator();
    }

    public void setDecimals(int decimals) {
        updateFormat("decimals", Integer.TYPE, decimals);
    }

    public int getDecimals() {
        return getDecimalFormat().getDecimals();
    }
    
    private void updateFormat(String prop, Class objClass, Object objValue) {
        DecimalFormat df = getDecimalFormat();
        try {
            Method m = df.getClass().getDeclaredMethod("set" + prop.substring(0,1).toUpperCase() + prop.substring(1), objClass);
            m.invoke(df, objValue);
        } catch (NoSuchMethodException | SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            throw new DecimalFormatException("Error setting " + prop + " with value " + objValue.toString(), ex);
        }
    }
    
    public void setDecimalFormat(DecimalFormat format) {
        getState().format = format;
    }
        
    public DecimalFormat getDecimalFormat() {
        return getState().format;
    }
        
}
