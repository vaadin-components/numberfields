package ar.com.zir.numberfields.demo;


import ar.com.zir.numberfields.DecimalTextField;
import ar.com.zir.numberfields.IntegerTextField;
import ar.com.zir.numberfields.shared.DecimalFormat;
import ar.com.zir.numberfields.validator.Decimal;
import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItem;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import java.math.BigDecimal;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.constraints.Max;

@Theme("demo")
@Title("NumberFields Add-on Demo")
@SuppressWarnings("serial")
public class DemoUI extends UI
{

    @WebServlet(value = "/*", asyncSupported = true)
    @VaadinServletConfiguration(productionMode = false, ui = DemoUI.class, widgetset = "ar.com.zir.numberfields.demo.DemoWidgetSet")
    public static class Servlet extends VaadinServlet {
    }

    @Override
    protected void init(VaadinRequest request) {
        TestBean testBean = new TestBean(new BigDecimal("3.15"));
        BeanItem<TestBean> bean = new BeanItem<>(testBean);
        BeanFieldGroup<TestBean> fg = new BeanFieldGroup(TestBean.class);
        fg.setItemDataSource(bean);
        // Initialize our new UI component
        final DecimalTextField component = new DecimalTextField("DecimalTextField");
        try {
            Decimal dec = fg.getItemDataSource().getBean().getClass().getDeclaredField("test").getAnnotation(Decimal.class);
            DecimalFormat df = new DecimalFormat();
            df.setDecimalSeparator(dec.decimalSeparator());
            df.setGroupingSeparator(dec.groupingSeparator());
            df.setGroupingSize(dec.groupingSize());
            df.setDecimals(dec.fraction());
            df.setMaxIntegers(dec.integer());
            component.setDecimalFormat(df);
        } catch (NoSuchFieldException | SecurityException ex) {
            Logger.getLogger(DemoUI.class.getName()).log(Level.SEVERE, null, ex);
        }
        component.setBigDecimalValue(new BigDecimal("3.15"));
        component.setImmediate(true);
        fg.bind(component, "test");
        
        final IntegerTextField component2 = new IntegerTextField("IntegerTextField");
//        try {
//            Max max = fg.getItemDataSource().getBean().getClass().getDeclaredField("test").getAnnotation(Max.class);
//            component2.setMaxIntegers(String.valueOf(max.value()).length());
//        } catch (NoSuchFieldException | SecurityException ex) {
//            Logger.getLogger(DemoUI.class.getName()).log(Level.SEVERE, null, ex);
//        }
        component2.setMaxIntegers(6);
        component2.setIntegerValue(new Integer("55"));
        component2.setImmediate(true);
//        fg.bind(component2, "test");

        final VerticalLayout layout = new VerticalLayout();

        Button button = new Button("Test");
        button.addClickListener(new ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                testBean.setTest(testBean.getTest().multiply(new BigDecimal("2")));
            }
        });

        Button result = new Button("Result");
        result.addClickListener(new ClickListener() {

            @Override
            public void buttonClick(Button.ClickEvent event) {
                Label lbl = new Label("Value: " + fg.getItemDataSource().getBean().getTest());
                layout.addComponent(lbl);
                layout.setComponentAlignment(lbl, Alignment.MIDDLE_CENTER);
            }
        });

        // Show it in the middle of the screen
        layout.setStyleName("demoContentLayout");
        layout.setSizeFull();
        layout.addComponent(component);
        layout.addComponent(component2);
        layout.addComponent(button);
        layout.addComponent(result);
        layout.setComponentAlignment(component, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(component2, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(button, Alignment.MIDDLE_CENTER);
        layout.setComponentAlignment(result, Alignment.MIDDLE_CENTER);
        setContent(layout);

    }
    
    public class TestBean {
        @Decimal(decimalSeparator = ",", fraction = 2, integer = 3, groupingSeparator = ".", groupingSize = 3)
        private BigDecimal test;

        public TestBean(BigDecimal test) {
            this.test = test;
        }

        public BigDecimal getTest() {
            return test;
        }

        public void setTest(BigDecimal test) {
            this.test = test;
            System.out.println("Value " + test);
        }
        
    }

}
